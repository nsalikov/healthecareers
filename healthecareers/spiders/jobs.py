# -*- coding: utf-8 -*-
import re
import json
from datetime import date
from urllib.parse import unquote
import scrapy


class JobsSpider(scrapy.Spider):
    name = 'jobs'
    allowed_domains = ['healthecareers.com']
    phone_pattern = re.compile('(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?', re.I)
    email_pattern = re.compile(r'mailto%3a(.*?)"', re.I)
    url = 'https://www.healthecareers.com/search-jobs/?ps=100&catid={catid}&locationtext={loc}'


    def start_requests(self):
        urls = self.gen_urls([''])
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_professions)


    def parse_professions(self, response):
        professions = self.get_professions(response)
        for catid in professions:
            url = self.gen_url(catid)
            d = {
                    'categorization': {
                        'profession': professions[catid]
                    }
                }

            yield scrapy.Request(url=url, meta=d, callback=self.parse_specialities)


    def parse_specialities(self, response):
        profession = response.meta['categorization']['profession']
        profession_id = profession['catid']

        specialities = self.get_specialities(response)
        subspecialities = self.get_subspecialities(response)

        spec_ids = [catid for catid in specialities if specialities[catid]['parent_id'] == profession_id]

        for spec_id in spec_ids:
            speciality = specialities[spec_id]

            prof = {'catid': profession['catid'], 'name': profession['name'], 'parent_id': profession['parent_id']}
            spec = {'catid': speciality['catid'], 'name': speciality['name'], 'parent_id': speciality['parent_id']}

            d = {
                    'categorization': {
                        'profession': prof,
                        'speciality': spec,
                        'subspeciality': None,
                    }
                }

            subspec_ids = [catid for catid in subspecialities if subspecialities[catid]['parent_id'] == spec_id]

            if subspec_ids:
                for subspec_id in subspec_ids:
                    subspeciality = subspecialities[subspec_id]
                    d['categorization']['subspeciality'] = {'catid': subspeciality['catid'], 'name': subspeciality['name'], 'parent_id': subspeciality['parent_id']}
                    url = self.gen_url(subspec_id)

                    yield scrapy.Request(url=url, meta=d, callback=self.parse_search)
            else:
                url = self.gen_url(spec_id)

                yield scrapy.Request(url=url, meta=d, callback=self.parse_search)


    def parse_search(self, response):
        pagination_urls = response.css('div.pagination-generator li a.pageControl ::attr(href)').extract()
        for url in pagination_urls:
            yield scrapy.Request(url=url, meta=response.meta, callback=self.parse_search)

        search_urls = response.css('#search-jobs-content a.job-title ::attr(href)').extract()
        for url in search_urls:
            # self.logger.info('Get url: <{url}> ({meta})'.format(url=url, meta=response.meta['categorization']))
            yield scrapy.Request(url=url, meta=response.meta, callback=self.parse_jobs)


    def parse_jobs(self, response):
        try:
            json_text = response.css('script[type="application/ld+json"] ::text').extract_first()
            d = json.loads(json_text)
        except:
            self.logger.warning('Unable to parse JSON in <{url}>'.format(url=response.request.url))
            return

        d['extra'] = {}
        d['extra']['categorization'] = response.meta['categorization']

        d['description'] = re.sub('[\t|\r|\n|\xa0]+', ' ', d['description'])
        d['description'] = re.sub('\s{2,}', ' ', d['description'])

        d['extra']['contact_information'] = self.get_contacts(d['description'])

        d['extra']['page_url'] = response.request.url
        d['extra']['date_read'] = date.today().isoformat()

        d['extra']['logo_url'] = response.css('div.employer-logo img ::attr(src)').extract_first()
        if d['extra']['logo_url'] and not d['extra']['logo_url'].startswith('http'):
            d['extra']['logo_url'] = response.urljoin(d['extra']['logo_url'])

        return d


    def gen_urls(self, ids=[''], locs=['']):
        for loc in locs:
            for catid in ids:
                yield self.gen_url(catid, loc)


    def gen_url(self, catid='', loc=''):
        return self.url.format(catid=catid, loc=loc)


    def get_professions(self, response):
        d = {}
        for t in response.css('#profession-dropdown ul li'):
            name = t.css('li ::text').extract_first()
            name = name.strip() if name else name

            catid = t.css('li ::attr(data_profession_id)').extract_first()
            if catid:
                catid = catid.strip() if catid else catid
                d[catid] = {'catid': catid, 'name': name, 'type': 'professions', 'parent_id': None}

        return d


    def get_specialities(self, response):
        d = {}
        for t in response.css('#specialty div.specialty-container input.search-facet-checkbox.tier2-checkbox'):
            parent_id = t.css('input ::attr(data-parent-id)').extract_first()
            parent_id = parent_id.strip() if parent_id else parent_id

            name = t.css('input ::attr(data-category-title)').extract_first()
            if name:
                name = name.strip() if name else name
                name = re.sub('\s*\(.*?\)\s*$', '', name)

            catid = t.css('input ::attr(data-category-id)').extract_first()
            if catid:
                catid = catid.strip() if catid else catid
                d[catid] = {'catid': catid, 'name': name, 'type': 'speciality', 'parent_id': parent_id}

        return d


    def get_subspecialities(self, response):
        d = {}
        for t in response.css('#specialty div.subspecialty-facet input.search-facet-checkbox.tier3-checkbox'):
            parent_id = t.css('input ::attr(data-parent-id)').extract_first()
            parent_id = parent_id.strip() if parent_id else parent_id

            name = t.css('input ::attr(data-category-title)').extract_first()
            if name:
                name = name.strip() if name else name
                name = re.sub('\s*\(.*?\)\s*$', '', name)

            catid = t.css('input ::attr(data-category-id)').extract_first()
            if catid:
                catid = catid.strip() if catid else catid
                d[catid] = {'catid': catid, 'name': name, 'type': 'subspecialty', 'parent_id': parent_id}

        return d


    def get_contacts(self, description):
        contacts = {'emails': [], 'phones': [], 'raw_data': None}

        contacts['emails'] = self.get_contacts_emails(description)
        contacts['phones'] = self.get_contacts_phones(description)
        contacts['raw_data'] = self.get_contacts_raw_data(description)

        return contacts


    def get_contacts_emails(self, description):
        emails = [unquote(e.strip()) for e in re.findall(self.email_pattern, description) if e]
        emails = [e.lower() for e in emails]

        return list(set(emails))


    def get_contacts_phones(self, description):
        phones = []

        pp = [list(x) for x in re.findall(self.phone_pattern, description)]
        for p in pp:
            if p[0] and not p[1]:
                p[1] = p[0]
                p[0] = ''
            if all([p[1], p[2], p[3]]):
                phone = '({}) {}-{}'.format(p[1], p[2], p[3])
                if p[0]:
                    phone = p[0] + ' ' + phone
                if p[4]:
                    phone = phone + ' ext ' + p[4]

                phones.append(phone)

        return list(set(phones))


    def get_contacts_raw_data(self, description):
        raw_data = None

        match = re.search('.*(<p><strong>Contact Information</strong></p>.*)', description, re.DOTALL)
        if match:
           raw_data = match.group(1)

        return raw_data