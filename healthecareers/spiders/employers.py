# -*- coding: utf-8 -*-
import re
from datetime import date
import scrapy


class EmployersSpider(scrapy.Spider):
    name = 'employers'
    allowed_domains = ['healthecareers.com']
    start_urls = [
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=A#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=B#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=C#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=D#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=E#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=F#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=G#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=H#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=I#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=J#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=K#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=L#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=M#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=N#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=O#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=P#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=Q#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=R#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=S#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=T#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=U#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=V#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=W#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=X#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=Y#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=Z#top',
        'https://www.healthecareers.com/healthcare-employers/?emplLetter=1#top',
    ]

    def parse(self, response):
        for url in response.css('div.grey-content a ::attr(href)'):
            yield response.follow(url, callback=self.parse_employers)


    def parse_employers(self, response):
        try:
            new_body = response.text.encode('latin1', 'backslashreplace').decode('unicode-escape').encode('utf-8')
            response = response.replace(body=new_body)
        except:
            pass

        title = response.css('div.job-title-etc h1 ::text').extract_first()
        title = title.strip() if title else title

        address = filter(None, [t.strip().strip(',') for t in response.css('div.job-title-etc h2 *::text').extract() if t])
        address = [re.sub('[\t|\r|\n|\xa0]+', ' ', a) for a in address]
        address = [re.sub('\s{2,}', ' ', a) for a in address]
        address = ', '.join(address)

        description = response.css('#empabout').extract_first()

        if description:
            description = re.sub('[\t|\r|\n|\xa0]+', ' ', description)
            description = re.sub('\s{2,}', ' ', description)

        page_url = response.request.url
        date_read = date.today().isoformat()

        logo_url = response.css('#empabout img ::attr(src)').extract_first()
        if not logo_url:
            logo_url = response.css('div.employer-logo img ::attr(src)').extract_first()
        if logo_url and not logo_url.startswith('http'):
            logo_url = response.urljoin(logo_url)

        d = {
                'title': title,
                'address': address,
                'description': description,
                'logo_url': logo_url,
                'page_url': page_url,
                'date_read': date_read
            }

        return d
