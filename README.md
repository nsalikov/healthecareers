# Healthecareers' Scraper

Scrapy scraper for [healthecareers.com](https://www.healthecareers.com).

## Getting Started

### Prerequisites

* [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [python 3](https://docs.python.org/3/using/index.html)
* [scrapy](https://doc.scrapy.org/en/latest/intro/install.html)
* [shub](https://helpdesk.scrapinghub.com/support/solutions/articles/22000204081-deploying-your-spiders-to-scrapy-cloud) (optionally, for deployment to scrapinghub.com)

### Installing

Clone project and change directory:

```
git clone https://nsalikov@bitbucket.org/nsalikov/healthecareers.git
cd healthecareers
```

### Configuration

Not required.

### Usage

Just run:

```
scrapy crawl employers
scrapy crawl jobs
```

To save output you need to specify command-line options:

```
# extensions matter
scrapy crawl employers -o employers.jl
scrapy crawl jobs -o jobs.jl

# to save the log as well (options are in the GNU style):
scrapy crawl employers --output=employers.jl --logfile=employers.log
scrapy crawl employers --output=jobs.jl --logfile=jobs.log
```